/*
// Solucion 1
let contador = 0;

document.addEventListener("click", (e) => {
    let contenido = "<div></div>";

    e.target.innerHTML += contenido;
    document.querySelectorAll("div")[contador].style.top = e.clientY + "px";
    document.querySelectorAll("div")[contador].style.left = e.clientX + "px";
    contador++;

});*/


// Solucion 2
document.addEventListener("click", (e) => {
    let contenido = `<div style="left:${e.clientX}px;top:${e.clientY}px"></div>`;

    e.target.innerHTML += contenido;

});



/*
document.addEventListener("click", (e) => {
    let elemento = document.createElement("div");
    
    e.target.appendChild(elemento);
    elemento.style.left = e.clientX + "px";
    elemento.style.top = e.clientY + "px";
});
*/