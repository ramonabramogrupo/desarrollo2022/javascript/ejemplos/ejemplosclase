document.addEventListener("click", function(e) {
    // objeto sobre el que se realiza clic
    console.log(e.target);

    //objeto que se le coloca el escuchador
    console.log(e.currentTarget);
    console.log(this);
    console.log(document);

    // si realizo clic sobre un td se coloca rojo
    if (e.target.tagName == "TD") {
        e.target.style.backgroundColor = "red";
    }
});