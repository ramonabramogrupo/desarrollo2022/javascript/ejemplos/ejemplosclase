let numero = 0;
let pares = 0;
let maximo = 0;
let minimo = 0;

/**
 * primera vuelta la realizo fuera del for
 */
numero = +prompt("Introduce numero");
if (numero % 2 == 0) {
    pares++;
}
minimo = numero;
maximo = numero;

for (let c = 0; c < 9; c++) {
    numero = +prompt("Introduce numero");
    if (numero % 2 == 0) {
        pares++;
    }
    if (numero > maximo) {
        maximo = numero;
    }
    if (numero < minimo) {
        minimo = numero;
    }
}

document.write(`Hay ${pares} numeros pares<br>`);
document.write(`El numero mayor es ${maximo}<br>`);
document.write(`El numero minimo es ${minimo}<br>`);