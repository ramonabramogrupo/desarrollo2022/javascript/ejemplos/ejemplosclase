let letras = ['T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 'N', 'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E', 'T'];

let dni = +prompt("Introduce tu dni");
let indice = 0;
let letra = '';

if (dni < 0 || dni > 99999999) {
    document.write("Numero dni incorrecto");
} else {
    indice = dni % 23;
    letra = letras[indice];
    document.write(`el dni completo es ${dni}-${letra}`);
}