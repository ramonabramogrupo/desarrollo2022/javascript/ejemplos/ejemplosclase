document
    .querySelector('button')
    .addEventListener("click", function() {
        let letras = ['T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 'N', 'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E', 'T'];

        let dni = +document.querySelector('#dni').value;
        let indice = 0;
        let letra = '';

        if (dni < 0 || dni > 99999999) {
            document.querySelector('#salida').innerHTML = "Numero dni incorrecto";
        } else {
            indice = dni % 23;
            letra = letras[indice];
            document.querySelector('#salida').innerHTML = `${dni}-${letra}`;
        }


    });