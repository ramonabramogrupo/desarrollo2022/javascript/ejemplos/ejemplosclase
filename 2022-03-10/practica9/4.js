let numeros = [5, 8];

if (numeros[0] <= numeros[1]) {
    console.log('Numero1 no es mayor que numero2');
}

if (numeros[1] > 0) {
    console.log('Numero2 es positivo');
}

if (numeros[0] < 0 || numeros[0] != 0) {
    console.log('Numero1 es negativo O distinto de cero');
}

if ((numeros[0] + 1) < numeros[1]) {
    console.log('Incrementar en una unidad numero1 no lo hace mayor o igual que numero2');
}