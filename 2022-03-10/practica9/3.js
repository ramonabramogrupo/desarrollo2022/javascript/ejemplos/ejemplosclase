let numero1 = 5;
let numero2 = 8;

if (numero1 <= numero2) {
    console.log('Numero1 no es mayor que numero2');
}

if (numero2 > 0) {
    console.log('Numero2 es positivo');
}

if (numero1 < 0 || numero1 != 0) {
    console.log('Numero1 es negativo O distinto de cero');
}

if ((numero1 + 1) < numero2) {
    console.log('Incrementar en una unidad numero1 no lo hace mayor o igual que numero2');
}