window.addEventListener("load", function() {
    // creo el elemento div de forma global
    let div = document.createElement("div");
    div.innerHTML = "hola mundo";

    document.querySelector("button").addEventListener("click", function() {
        // cada vez que pulso el boton me coloca
        // en salida un clon del div original
        document.querySelector("#salida").appendChild(div.cloneNode(true));
    });
});