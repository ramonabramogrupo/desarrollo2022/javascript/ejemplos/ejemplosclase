let foto = document.querySelector('#original');

document.querySelector("button").addEventListener("click", () => {
    // creo un duplicado de la foto original
    let nueva = foto.cloneNode();

    //añado el duplicado al body
    document.querySelector('body').appendChild(nueva);
});