window.addEventListener("load", function() {
    // este codigo se ejecuta cuando se cargue la web
    document.querySelector('button').addEventListener("click", function() {
        // este codigo se ejecuta cuando pulso el boton
        /*document.querySelector('#salida').innerHTML+="<div>Hola Mundo</div>"; // esto seria hasta ahora */

        // creo un div
        let div = document.createElement("div");

        // creo un nodo de texto
        let texto = document.createTextNode("Hola mundo");

        // añado texto al div
        //div.innerHTML = "Hola mundo";
        //div.textContent = "Hola mundo";
        div.appendChild(texto);

        document.querySelector('#salida').appendChild(div);


    });

});







/*
window.addEventListener("load",function(){
    document.querySelector("button").addEventListener("click",function(){
        let div=document.createElement("div");
        div.innerHTML="hola mundo";
        document.querySelector("#salida").appendChild(div);
    });
});*/