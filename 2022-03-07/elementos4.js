document.querySelector("button").addEventListener("click", () => {
    // creo un fragmento de codigo
    const FRAGMENTO = document.createDocumentFragment();

    // creo una etiqueta div
    // const FRAGMENTO=document.createElement("div");


    for (let c = 1; c <= 3; c++) {
        // creo una etiqueta img
        const FOTO = document.createElement("img");
        // coloco el src de la imagen
        FOTO.src = `img/${c}.jpg`;
        //añado la foto al fragmento
        FRAGMENTO.appendChild(FOTO);
    }

    //añado al body el fragmento
    document.querySelector('body').appendChild(FRAGMENTO);
});