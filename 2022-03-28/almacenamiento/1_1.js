window.addEventListener("load", () => {
    document.querySelector('div').innerHTML = localStorage.getItem("numero");

    document.querySelector('#borra').addEventListener("click", () => {
        localStorage.removeItem("numero");
        document.querySelector('div').innerHTML = localStorage.getItem("numero");
    });


    document.querySelector('#mas').addEventListener("click", () => {
        let n=[];
        if (localStorage.getItem("numero") != null) {
            n=JSON.parse(localStorage.getItem("numero"));
        }
        n.push(document.querySelector("#numero").value);

        localStorage.setItem("numero", JSON.stringify(n));
        document.querySelector('div').innerHTML = n;
    })
})