window.addEventListener("load", () => {
    document.querySelector('div').innerHTML = localStorage.getItem("dato");

    document.querySelector('#borra').addEventListener("click", () => {
        localStorage.removeItem("dato");
        document.querySelector('div').innerHTML = localStorage.getItem("dato");
    });


    document.querySelector('#mas').addEventListener("click", () => {
        let texto = document.querySelector('#nombre').value;
        if (localStorage.getItem("dato") != null) {
            texto = localStorage.getItem("dato") + texto;
        }

        localStorage.setItem("dato", texto);
        document.querySelector('div').innerHTML = texto;
    })
})