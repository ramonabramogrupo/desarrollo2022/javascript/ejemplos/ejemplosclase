class Dato {
    constructor(id = 0, nombre = '', edad = 0, poblacion = '', email = '') {
        this.id = id;
        this.nombre = nombre;
        this.edad = edad;
        this.poblacion = poblacion;
        this.email = email;
    }

    imprimir(etiqueta) {
        let resultado = "";
        resultado += `
        <${etiqueta}>${this.id}</${etiqueta}>
        <${etiqueta}>${this.nombre}</${etiqueta}>
        <${etiqueta}>${this.edad}</${etiqueta}>
        <${etiqueta}>${this.poblacion}</${etiqueta}>
        <${etiqueta}>${this.email}</${etiqueta}>
        `;
        return resultado;

    }

    static cabeceras() {
        let resultado = "";
        for (let titulo in new this()) {
            resultado += `<td>${titulo}</td>`;
        }
        return resultado;
    }

}

class Datos {
    constructor() {
        this.registros = [];
    }

    imprimir(etiqueta = "tr", etiqueta1 = 'td', inicio = 0, fin = this.registros.length) {
        let resultado = "";
        /* comprobar cuantos registros hay */
        if(this.registros.length<inicio){
            resultado="";
        }else{ 
            if(this.registros.length<fin){
                fin=this.registros.length;
            }
            for (let indice = inicio; indice < fin; indice++) {
                let registro = this.registros[indice].imprimir(etiqueta1);

                resultado += `<${etiqueta}>${registro}</${etiqueta}>`
            }
        }
        return resultado;
    }

    parse(){
        /*
        realizo este metodo para que los objetos 
        de tipo Dato cuando los recupere de la serializacion
        vengan como objetos de tipo Dato y con su metodo imprimir
        */
        this.registros.forEach((v,i)=>{
            this.registros[i]=new Dato(
                v.id,
                v.nombre,
                v.edad,
                v.poblacion,
                v.email
            )
        });
    }
}

setInterval(function(){

fetch(
    //'http://formularios.javascript.ramonabramo.es/listarUsuarios.php'
    'http://127.0.0.1/2022/desarrollo/ejemplos/rest/rest.php'
    )
    .then(response => {
        if(response.ok){
            return response.json();
        }else{
            console.log('error');
        }
         
    })
    .then(valores => {
        let datos = new Datos();
        // recupero los datos del servidor
        // y los almaceno en el objeto DATOS en 
        //  la propiedad registros
        datos.registros = valores;
        // añado a cada uno de los registros los metodos del objeto DATO
        datos.parse();
        // imprimo los datos
        document.querySelector('tbody').innerHTML = datos.imprimir();
        // coloco las cabeceras a la tabla
        let header = Dato.cabeceras();
        document.querySelector('thead>tr').innerHTML = header;
    });
},1000);

    document.querySelector('#nuevo').addEventListener("click", function(e) {
        const data = new FormData(document.querySelector('form'));

        fetch(
            //'http://formularios.javascript.ramonabramo.es/insertar.php'
            'http://127.0.0.1/2022/desarrollo/ejemplos/rest/rest.php'
            , {
                method: 'post',
                body: data
            })
            .then(function(response) {
                if (response.ok) {
                    return response.text()
                } else {
                    throw "Error en la llamada Ajax";
                }
    
            })
            .then(function(texto) {
                alert(texto);
            })
            .catch(function(err) {
                alert(err);
            });

            fetch(
                //'http://formularios.javascript.ramonabramo.es/listarUsuarios.php'
                'http://127.0.0.1/2022/desarrollo/ejemplos/rest/rest.php'
                )
                .then(response => {
                    if(response.ok){
                        return response.json();
                    }else{
                        console.log('error');
                    }
                     
                })
                .then(valores => {
                    let datos = new Datos();
                    // recupero los datos del servidor
                    // y los almaceno en el objeto DATOS en 
                    //  la propiedad registros
                    datos.registros = valores;
                    // añado a cada uno de los registros los metodos del objeto DATO
                    datos.parse();
                    // imprimo los datos
                    document.querySelector('tbody').innerHTML = datos.imprimir();
                    // coloco las cabeceras a la tabla
                    let header = Dato.cabeceras();
                    document.querySelector('thead>tr').innerHTML = header;
                });
    });

    