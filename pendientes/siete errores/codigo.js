// JavaScript Document

var fotos = new Array();
var aciertos = 0;
var contador = 0;
var reloj;

window.addEventListener("load", cargar);

function cargar(){
	document.querySelector("#tiempo").style.width="1000px";
	for(var i=0;i<3;i++){
		fotos[i]={};
		fotos[i].bien = new Image();
		fotos[i].bien.src = "fotos/"+(i+1)+"bien.png";
		fotos[i].mal = new Image();
		fotos[i].mal.src = "fotos/"+(i+1)+"mal.png";
		console.log(fotos[i].bien);
		console.log(fotos[i].mal);
		
	}
	
	/*var figuras = document.querySelectorAll("section>figure");
	figuras[0].innerHTML = "<img src='"+fotos[0].bien.src+"'>";
	figuras[1].innerHTML += "<img src='"+fotos[0].mal.src+"'>";*/
	nueva();
	
	reloj=setInterval(tiempo, 100);
	/*document.querySelector("#tiempo").style.width="0px";
	document.querySelector("#tiempo").addEventListener("transitionend", timeout);*/
	
	errores(0);
	
}

function tiempo(){
	var x = parseInt(document.querySelector("#tiempo").style.width);
	if(x>0){
		x--;
		document.querySelector("#tiempo").style.width = x+"px";
	}else{
		clearInterval(reloj);
		timeout();
	}
}

function timeout(){
	alert("Se te acabo el tiempo. Has completado "+contador+" laminas.");
	
	var figuras = document.querySelectorAll("figure");
	figuras[1].innerHTML = "";
}


function errores(indice){
	
	document.querySelector("#uno").className = "uno"+(indice+1);
			
			document.querySelector("#dos").className = "dos"+(indice+1);
			
			document.querySelector("#tres").className = "tres"+(indice+1);
			
			document.querySelector("#cuatro").className = "cuatro"+(indice+1);
			
			document.querySelector("#cinco").className = "cinco"+(indice+1);
	
	/*switch(indice){
		case 0:
			document.querySelector("#uno").className = "uno1";
			
			document.querySelector("#dos").className = "dos1";
			
			document.querySelector("#tres").className = "tres1";
			
			document.querySelector("#cuatro").className = "cuatro1";
			
			document.querySelector("#cinco").className = "cinco1";
			break;
		case 1:
			document.querySelector("#uno").className = "uno2";
			
			document.querySelector("#dos").className = "dos2";
			
			document.querySelector("#tres").className = "tres2";
			
			document.querySelector("#cuatro").className = "cuatro2";
			
			document.querySelector("#cinco").className = "cinco2";
			break;
		case 2:
			document.querySelector("#uno").className = "uno3";
			
			document.querySelector("#dos").className = "dos3";
			
			document.querySelector("#tres").className = "tres3";
			
			document.querySelector("#cuatro").className = "cuatro3";
			
			document.querySelector("#cinco").className = "cinco3";
			break;
	}*/
	
	var error = document.querySelectorAll("figure>div");
	
	for(var i=0;i<error.length;i++){
		error[i].addEventListener("click", correcto);
	}
	
	document.querySelectorAll("figure>img")[1].addEventListener("click", fallo);
}

function correcto(event){
	var capa = event.target;
	console.log(capa);
	capa.style.backgroundImage = "url('fotos/tic.png')";
	capa.removeEventListener("click", correcto);
	
	aciertos++;
	if (aciertos==5){
		aciertos=0;
		setTimeout(function(){nueva(++contador)}, 1000);
	}
}

function fallo(){
	
	var x = parseInt(document.querySelector("#tiempo").style.width);
	x-=50;
	document.querySelector("#tiempo").style.width = x + "px";
}

function nueva(){
	var figuras = document.querySelectorAll("section>figure");
	
	if(contador<3){
		figuras[0].innerHTML = "<img src='"+fotos[contador].bien.src+"'>";
		figuras[1].innerHTML = "<div id='uno'></div><div id='dos'></div><div id='tres'></div><div id='cuatro'></div><div id='cinco'></div><img src='"+fotos[contador].mal.src+"'>";
	}else{
		fin();
	}
	
	errores(contador);
}

function fin(){
	document.querySelector("#tiempo").style.width=document.querySelector("#tiempo").style.width;
	document.querySelector("#tiempo").style.transition="none";
	//document.querySelector("#tiempo").transitionend();
	clearInterval(reloj);
	alert("Enorabuena has completado " + contador + " laminas.");
}