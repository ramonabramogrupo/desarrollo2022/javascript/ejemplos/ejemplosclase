// creo una variable

a = "Hola Ramon";

// utilizando plantillas de strings
document.write(`<h1>${a}</h1>`);

// utilizando el operador de concatenar
document.write("<h1>" + a + "</h1>");