/**
 * definicion de variables
 */

let base = 0;
let altura = 0;
let area = 0;
let perimetro = 0;

/**
 * introducir datos
 */

base = prompt("Introduce la base");
altura = prompt("Introduce la altura");

/**
 * procesamiento de la informacion
 */

area = base * altura;
perimetro = 2 * base + 2 * altura;

/**
 * Mostrar la informacion
 */

document.write("<div>El area es " + area + "</div>")
document.write(`<div>El perimetro es ${perimetro}</div>`);