/* Vamos a crear una clase padre */

class Persona {
    // propiedad privada
    #nombre = "";

    // metodos publicos
    dormir() {
        console.log("ZZZZZZZZZZZZ");
    };

    hablar() {
        console.log("BLA BLA BLA");
    };

    contar() {
        console.log("1 2 3 4 5 6");
    };

    // setter
    set nombre(valor) {
        this.#nombre = valor;
    };

    //getter
    get nombre() {
        return this.#nombre;
    };

}

/* Vamos a crear una clase hija */

class Hija extends Persona {
    constructor() {
        super();
        this.atributoHijo = 18;

    }

}

/* vamos a crear un objeto desde la clase hija */

var Ana = new Hija();

/* Vamos a crear un objeto desde la clase padre */

var Ramon = new Persona();

console.log(Ana.atributoHijo);
console.log(Ana);
console.log(Ramon);