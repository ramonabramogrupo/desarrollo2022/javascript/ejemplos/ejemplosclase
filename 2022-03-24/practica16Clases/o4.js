/* 
	Creamos la clase
*/

class Caja{
    // propiedad privada
    #unidad="px";
    // metodo constructor
    constructor(alto,ancho,texto){
        // propiedades publicas
        this.ancho=alto;
        this.alto=ancho;
        this.texto=texto;
    }

    // metodo privado
    concatenar(){
        this.ancho=this.ancho+this.#unidad;
		this.alto=this.alto+this.#unidad;
    }

    //metodos publicos
	mensaje(){
		this.texto="esto es un ejemplo";
	};
	
    mostrar(){
		alert(this.texto);
	};


}
/*
	Creamos el objeto
*/

const objeto=new Caja(10,20,"hola mundo");

console.log(objeto.alto);
objeto.mostrar();