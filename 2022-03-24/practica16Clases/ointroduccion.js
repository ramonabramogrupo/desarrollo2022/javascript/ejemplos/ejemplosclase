/* Vamos a crear una clase */

class Persona{
	// propiedad privada
    #nombre="";
    // metodos publicos
	dormir(){
		console.log("ZZZZZZZZZZZZ");
	};
	
	hablar(){
		console.log("BLA BLA BLA");
	};
	
	contar(){
		console.log("1 2 3 4 5 6");
	};

    // setter
	set nombre(valor){
		this.#nombre=valor;
	};
	
    //getter
	get nombre(){
		return this.#nombre;
	};
}

/* vamos a crear un objeto con una instancia de la clase */
const alumno=new Persona();

alumno.dormir();
alumno.hablar();
alumno.contar();
alumno.nombre="ramon";
console.log(alumno.nombre);