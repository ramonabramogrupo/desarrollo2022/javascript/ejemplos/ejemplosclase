/* Vamos a crear una clase padre */

const Persona=function(){
	// propiedad privada
	let nombre;
	// metodos publicos
	this.dormir=function(){
		console.log("ZZZZZZZZZZZZ");
	};
	
	this.hablar=function(){
		console.log("BLA BLA BLA");
	};
	
	this.contar=function(){
		console.log("1 2 3 4 5 6");
	};
	
	this.setNombre=function(valor){
		nombre=valor;
	};
	
	this.getNombre=function(){
		return nombre;
	};
};

/* Vamos a crear una clase hija */

const Hija=function(){
	this.atributoHijo=18;
};

// para que Hija herede todo lo de Persona
Hija.prototype=new Persona();

/* vamos a crear un objeto desde la clase hija */

const ana=new Hija();

/* Vamos a crear un objeto desde la clase padre */

const ramon=new Persona();

console.log(ana.atributoHijo);
console.log(ana);
console.log(ramon);

