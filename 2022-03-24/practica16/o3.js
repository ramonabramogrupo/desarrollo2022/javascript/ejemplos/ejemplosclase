/* Vamos a crear una clase que recibe 3 argumentos */
const Caja=function(a,b,c){
	// propiedades publicas
	this.ancho=a;
	this.alto=b;
	this.texto=c;
	//metodos publicos
	this.mensaje=function(){
		this.texto="esto es un ejemplo";
	};
	this.mostrar=function(){
		alert(this.texto);
	};
};

