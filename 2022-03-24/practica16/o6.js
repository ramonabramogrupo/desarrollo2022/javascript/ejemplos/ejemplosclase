/*
	Creamos el objeto
*/

const persona = {
    nombre: "ramon",
    edad: 20,
    decirNombre: function() {
        console.log(this.nombre);
    }
};

persona.decirNombre();