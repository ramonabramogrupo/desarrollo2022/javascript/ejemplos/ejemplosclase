function leer() {
    let vCaracter = document.querySelector("#caracter");
    let vPalabra = document.querySelector("#palabra");
    let vSalida = document.querySelector("#salida");
    let palabra = vPalabra.value; // palabra escrita
    let caracter = vCaracter.value; // caracter a buscar
    let posiciones = [];
    let posicion = 0;


    // busca la primera ocurrencia del caracter buscado
    posicion = palabra.indexOf(caracter);
    while (posicion != -1) {
        posiciones.push(posicion)
        posicion = palabra.indexOf(caracter, posicion + 1)
    }


    // mostrando en que posiciones esta el caracter buscado
    vSalida.innerHTML = posiciones.join(",");
}