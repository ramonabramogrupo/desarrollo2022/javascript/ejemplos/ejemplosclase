let frase = "Ejemplo de clase"

// creo un array con 5 elementos por
// cada vocal
let repeticiones = new Array(5);
repeticiones.fill(0);

frase = frase.split("");

frase.forEach(function(caracter) {
    switch (caracter) {
        case 'a':
            repeticiones[0]++;
            break;
        case 'e':
            repeticiones[1]++;
            break;
        default:
            break;
    }
});