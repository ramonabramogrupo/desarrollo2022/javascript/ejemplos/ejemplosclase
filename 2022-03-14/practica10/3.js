let numeros = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0];

/**
 * pedir los numeros por teclado
 */
/*let numeros=[];
for(let c=0;c<10;c++){
    //numeros.push(+prompt("Introduce un numero"));
    numeros[c]=+prompt("Introduce un numero")
}*/

// opcion 1 
// recorriendo el array del final al principio

/*for (let c = numeros.length - 1; c >= 0; c--) /{
    document.write(`${numeros[c]}<br>`);
}*/

// opcion 2
// utilizamos el metodo reverse 
// para girar el array
// y el metodo join para imprimirlo

numeros.reverse();

document.write(numeros.join("<br>"));