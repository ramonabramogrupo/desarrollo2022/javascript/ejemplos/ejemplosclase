//'use strict';

// colocando un acceso a las celdas
// don quiero escribir
let celdas = document.querySelectorAll('#salida>td');

let numeros = [1, 2, 3, 4, 5, 6];

let pares = [];

let maximo = 0;

let minimo = 0;


/*
// Realizado con foreach
numeros.forEach(function(valor) {
    if (valor % 2 == 0) {
        pares.push(valor);
    }
});
*/

// Realizado con filter
pares = numeros.filter(function(valor) {
    return (valor % 2 == 0);
});


maximo = Math.max(...numeros);
minimo = Math.min(...numeros);

celdas[0].innerHTML = numeros.join("<br>")
celdas[1].innerHTML = pares.join("<br>");
celdas[2].innerHTML = maximo;
celdas[3].innerHTML = minimo;