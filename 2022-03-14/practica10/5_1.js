let numeros = [-2, -3, 0, 2, 4, 6];
let positivos = 0;
let negativos = 0;
let valoresPositivos = [];
let valoresNegativos = [];

valoresPositivos = numeros.filter(function(v) {
    return v > 0;
});

valoresNegativos = numeros.filter(function(v) {
    return v < 0;
});

valoresPositivos.forEach(function(v) {
    positivos += v;
})

valoresNegativos.forEach(function(v) {
    negativos += v;
});


document.write(`La suma de los positivos es ${positivos}`);

document.write(`La suma de los negativos es ${negativos}`);