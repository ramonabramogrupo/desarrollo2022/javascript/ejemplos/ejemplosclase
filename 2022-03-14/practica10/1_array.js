let numeros = [];

let pares = 0;
let maximo = 0;
let minimo = 0;

for (let c = 0; c < 3; c++) {
    numeros[c] = +prompt("Introduce numero");
}

numeros.forEach(function(numero) {
    if (numero % 2 == 0) {
        pares++;
    }
});

//maximo = numeros.sort(function(a, b) { return b - a })[0];
//minimo = numeros.sort(function(a, b) { return a - b })[0];

maximo = Math.max(...numeros);
minimo = Math.min(...numeros);

document.write(numeros);
document.write(`<br>Hay ${pares} numeros pares<br>`);
document.write(`El numero mayor es ${maximo}<br>`);
document.write(`El numero minimo es ${minimo}<br>`);