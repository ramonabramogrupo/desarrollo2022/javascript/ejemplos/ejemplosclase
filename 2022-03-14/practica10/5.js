let numeros = [-2, -3, 0, 2, 4, 6];
let positivos = 0;
let negativos = 0;

numeros.forEach(function(valor) {
    if (valor > 0) {
        positivos += valor;
    } else {
        negativos += valor;
    }
})

document.write(`La suma de los positivos es ${positivos}`);

document.write(`La suma de los negativos es ${negativos}`);