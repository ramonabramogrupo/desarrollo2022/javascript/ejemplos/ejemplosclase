class Caja{
    constructor(datos={}){
        datos.hasOwnProperty("ancho") || (datos.ancho="diez");
        this.ancho=datos.ancho;
    }

    get ancho(){
        return this._ancho.toUpperCase();
    }

    set ancho(valor){
        this._ancho=valor;
    }

    dibujar(){
        return this.ancho;
    }
}

const caja1=new Caja({
    ancho:"cien"
});

console.log(caja1.ancho);
