class Dato {
    constructor(id = 0, nombre = '', edad = 0, poblacion = '', email = '') {
        this.id = id;
        this.nombre = nombre;
        this.edad = edad;
        this.poblacion = poblacion;
        this.email = email;
    }

    imprimir(etiqueta) {
        let resultado = "";
        resultado += `
        <${etiqueta}>${this.id}</${etiqueta}>
        <${etiqueta}>${this.nombre}</${etiqueta}>
        <${etiqueta}>${this.edad}</${etiqueta}>
        <${etiqueta}>${this.poblacion}</${etiqueta}>
        <${etiqueta}>${this.email}</${etiqueta}>
        `;
        return resultado;

    }

    static cabeceras() {
        let resultado = "";
        let etiqueta = 'td';
        resultado += `
        <${etiqueta}>id</${etiqueta}>
        <${etiqueta}>nombre</${etiqueta}>
        <${etiqueta}>edad</${etiqueta}>
        <${etiqueta}>poblacion</${etiqueta}>
        <${etiqueta}>email</${etiqueta}>
        `;
        return resultado;

        /*for (let titulo in new this()) {
            resultado += `<td>${titulo}</td>`;
        }
        return resultado;*/
    }

}

class Datos {
    constructor() {
        this.registros = [];
    }

    imprimir(etiqueta = "tr", etiqueta1 = 'td', inicio = 0, fin = this.registros.length) {
        let resultado = "";
        for (let indice = inicio; indice < fin; indice++) {
            let registro = this.registros[indice].imprimir(etiqueta1);

            resultado += `<${etiqueta}>${registro}</${etiqueta}>`
        }
        return resultado;
    }
}

let datos = new Datos();


/*function cabeceras(objeto) {
    let resultado = "";
    for (let titulo in objeto) {
        resultado += `<td>${titulo}</td>`;
    }
    return resultado;
}*/

let header = Dato.cabeceras();
document.querySelector('thead>tr').innerHTML = header;

document.querySelector('#nuevo').addEventListener("click", function(e) {

    let dato = new Dato(
        document.querySelector('#id').value,
        document.querySelector('#nombre').value,
        document.querySelector('#edad').value,
        document.querySelector('#poblacion').value,
        document.querySelector('#email').value
    );


    document.querySelector('tbody').innerHTML += `
    <tr>${dato.imprimir('td')}</tr>`;

    datos.registros.push(dato);
    console.log(datos.registros);


});


document.querySelector('#todos').addEventListener("click", function() {
    document.querySelector('tbody').innerHTML = datos.imprimir();

});

document.querySelector('#cuatro').addEventListener("click", function() {
    document.querySelector('tbody').innerHTML = datos.imprimir('tr', 'td', 0, 4);
});

document.querySelector('#ocho').addEventListener("click", function() {
    document.querySelector('tbody').innerHTML = datos.imprimir('tr', 'td', 4, 8);
});

document.querySelector('#resto').addEventListener("click", function() {
    document.querySelector('tbody').innerHTML = datos.imprimir('tr', 'td', 8);
});