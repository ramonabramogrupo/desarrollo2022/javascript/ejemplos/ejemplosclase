let direccion = 'derecha';
const PASO = 10;

let estilos = window.getComputedStyle(document.querySelector("#cursor"));
document.querySelector("#cursor").style.left = estilos.left;
document.querySelector("#cursor").style.top = estilos.top;

/** movimiento del cursor */
let intervalo = setInterval(function() {
    let cursor = document.querySelector('#cursor');

    switch (direccion) {
        case 'derecha':
            cursor.style.left = parseInt(cursor.style.left) + PASO + "px";
            if (parseInt(cursor.style.left) >= 500) {
                clearInterval(intervalo);
                alert("Muerto");
            }
            break;
        case 'izquierda':
            cursor.style.left = parseInt(cursor.style.left) - PASO + "px";
            if (parseInt(cursor.style.left) <= 0) {
                clearInterval(intervalo);
                alert("Muerto");
            }
            break;
        case 'arriba':
            cursor.style.top = parseInt(cursor.style.top) - PASO + "px";
            if (parseInt(cursor.style.top) <= 0) {
                clearInterval(intervalo);
                alert("Muerto");
            }
            break;
        case 'abajo':
            cursor.style.top = parseInt(cursor.style.top) + PASO + "px";
            if (parseInt(cursor.style.top) >= 500) {
                clearInterval(intervalo);
                alert("Muerto");
            }
            break;

    }
}, 100);

/** controlar tecla */
window.addEventListener("keydown", e => {
    switch (e.key) {
        case 'ArrowDown':
            direccion = 'abajo';
            break;
        case 'ArrowUp':
            direccion = 'arriba';
            break;
        case 'ArrowLeft':
            direccion = 'izquierda';
            break;
        case 'ArrowRight':
            direccion = 'derecha';
            break;
    }
})