let divs = document.querySelectorAll('div');

// utilizamos un for
for (let c = 0; c < divs.length; c++) {
    console.log(divs[c].innerHTML);
}

// utilizamos el for of
for (let valor of divs) {
    console.log(valor.innerHTML);
}

// utilizamos el for in
for (let indice in divs) {
    console.log(divs[indice].innerHTML);
}

// utilizamos foreach
divs.forEach(function(valor, indice) {
    console.log(valor.innerHTML);
});