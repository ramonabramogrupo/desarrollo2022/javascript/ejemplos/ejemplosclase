/**
 * Crear un clase denominada Perro
 * Perro1 nomenclatura antigua
 * Perro2 nomenclatura moderna (ES5)
 * 
 */

/**
 * propiedades
 * 
 * color
 * pelo
 * peso
 * fechaNacimiento
 * 
 */

/**
 * Metodos
 * 
 * ladrar ("gua gua");
 * dormir("durmiendo")
 * 
 */

let Perro1 = function() {
    // metodo constructor
    this.constructor = function() {
        this.color = "Negro";
        this.peso = 5;
        this.pelo = true;
        this.fechaNacimiento = "2020/1/1";
    }

    // metodos
    this.ladrar = function() {
        return "Gua Gua";
    }

    this.dormir = function() {
        return "Durmiendo";
    }

    this.constructor();
}

// creamos un objeto de tipo Perro1
let miPerro = new Perro1();

class Perro2 {
    constructor() {
        this.color = "Negro";
        this.peso = 5;
        this.pelo = true;
        this.fechaNacimiento = "2020/1/1";
    }

    ladrar() {
        return "Gua Gua";
    }

    dormir() {
        return "durmiendo";
    }
}

let otroPerro = new Perro2();